<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Socialite;
use App\User;

class SocialController extends Controller
{
  public function redirectToProvider($provider)
  {
      return Socialite::driver($provider)->redirect();
  }
   
  public function handleProviderCallback($provider)
  {
             
      $getInfo = Socialite::driver($provider)->user();
  
      // dd($getInfo);
       
      $user = $this->createUser($getInfo,$provider);
   
      auth()->login($user);
   
      return redirect()->to('/home');
   
  }
  function createUser($getInfo,$provider){
   
   $user = User::where('provider_id', $getInfo->id)->first();
   
   if (!$user) {
       $user = User::create([
          'name'     => $getInfo->name,
          'email'    => $getInfo->email,
          'provider_name' => $provider,
          'provider_id' => $getInfo->id,
          'remeber_token' => $getInfo->token,
      ]);
    }
    return $user;
  }
}
