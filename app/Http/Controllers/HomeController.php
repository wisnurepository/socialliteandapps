<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Umur;
use App\Rumus;
use Auth;
use App\Jobs\MailSend;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $rumus = Rumus::all();
        $data = Umur::where('user_id',$user->id)->first();
        if($user)
        {
          if($data->umur >= 40)
          {
            return view('oldpeople',compact('rumus'));
          }
          else
          {
            return view('milenialpeople',compact('rumus'));
          }
        }
        else
        {
          return view('home');
        }
    }

    public function umur(Request $request)
    {
      $newUmur = new Umur;
      $newUmur->user_id = Auth::user()->id;
      $newUmur->umur = $request->umur;
      $newUmur->save();

      if($request->umur >= 40)
      {
        return redirect('/')->with('success','Berhasil menambahkan umur');
      }
      elseif($request->umur <= 39)
      {
        return redirect('/')->with('success','Berhasil menambahkan umur');
      }

    }

    public function mail(Request $request)
    {
      $data = [
        'email' => $request->email,
        'subject' => $request->subject,
        'text' => $request->text,
      ];

      dispatch(new MailSend($data));
      return redirect()->back()->with('success','Email Berhasil dikirim');
    }

    public function whatsapp(Request $request)
    {
      // dd($request->all());
      $key='f1d5c602cf34554b88e90f015620c33cbb66ef53a322d4b7'; //this is demo key please change with your own key
      $url='http://116.203.92.59/api/send_message';
      $data = array(
        "phone_no"=> '+62'.$request->tlpn,
        "key"		=>$key,
        "message"	=> $request->text
      );
      $data_string = json_encode($data);

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_VERBOSE, 0);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
      curl_setopt($ch, CURLOPT_TIMEOUT, 360);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
      );
      $res=curl_exec($ch);
      if($res == 'Success')
      {
        return redirect()->back()->with('success','Whatsapp Berhasil dikirim');
      }
      else
      {
        return redirect()->back()->with('error','Whatsapp Terjadi kesalahan');
      }
      curl_close($ch);
    }
}
