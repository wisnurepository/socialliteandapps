<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\EmailCustom;
use Mail;

class MailSend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
      $this->data = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      // dd($this->data['email']);

      $email = new EmailCustom($this->data);
      Mail::to($this->data['email'])->send($email);
    }
}
