<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
  
  <div class="container-fluid p-0 m-0">
    <nav class="navbar  navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand offset-md-3" href="#">Socialite Page</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>      
      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="#"></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"></a>
          </li>
          <li class="nav-item">
          </li>
        </ul>
      </div>
    </nav>
    <br>
    <div class="col-10 offset-3">
      <div class="col-8">
        @if (session('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('error'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Error</span> {{session('error')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        
        <div class="card">
          <div class="card-header">
            Send Message
            
            <a class="btn btn-danger float-right" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </div>
          <div class="card-body">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Email</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Whatsapp</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Hitung Rumus</a>
              </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <form action="/email" method="post">
                  @csrf
                  <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="text" name="subject" class="form-control" placeholder="Subject">
                  </div>
                  <div class="form-group">
                    <textarea name="text" class="form-control" cols="30" rows="10"></textarea>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success btn-md float-right">Kirim Pesan</button>
                  </div>
                </form>
              </div>
              <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                
                <form action="/whatsapp" method="post">
                  @csrf
                  <div class="form-group">
                    <input type="number" name="tlpn" class="form-control" placeholder="Telepon">
                  </div>
                  <div class="form-group">
                    <textarea name="text" class="form-control" cols="30" rows="10"></textarea>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success btn-md float-right">Kirim Pesan</button>
                  </div>
                </form>

              </div>
              <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Rumus</label>
                  </div>
                  <select class="custom-select" id="rumus">
                    <option value="" selected>Pilih...</option>
                    @foreach($rumus as $item)
                      <option value="{{$item->id}}">{{$item->nm_rumus}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group row" id="lebar">
                  <div class="col-12">
                    <div class="col-12">
                      <input type="number" class="form-control" id="panjang" placeholder="Panjang">
                    </div>
                    <br>
                    <div class="col-12 text-center">
                      X
                    </div>
                    <br>
                    <div class="col-12">
                      <input type="number" class="form-control" id="lebarr" placeholder="Lebar">
                    </div>                    
                    <br>
                    <div class="col-12 text-center">
                      =
                    </div>
                    <br>
                    <div class="col-12">
                      <input type="number" class="form-control" id="jumlahLebar" placeholder="Jumlah">
                    </div>
                    <br>
                  </div>
                    <button id="btnjumlahlebar" class="btn btn-success btn-block mt-3 mb-3">Hitung</button>
                    <button id="btnresetlebar" class="btn btn-danger btn-block mt-1 mb-3">Reset</button>
                  
                </div>

                <div class="form-group row" id="lingkar">
                  <div class="col-12">
                    <div class="col-12">
                      <input type="text" class="form-control" id="n" value="3.14" readonly>
                    </div>
                    <br>
                    <div class="col-12 text-center">
                      X
                    </div>
                    <br>
                    <div class="col-12">
                      <input type="number" class="form-control" id="lebar_lingkaran" placeholder="Lebar">
                    </div>                    
                    <br>
                    <div class="col-12 text-center">
                      =
                    </div>
                    <br>
                    <div class="col-12">
                      <input type="number" class="form-control" id="jmlhLingkaran" placeholder="Jumlah">
                    </div>
                    <br>
                  </div>
                    <button type="submit" id="btnjumlahLingkaran" class="btn btn-success btn-block mt-3 mb-3">Hitung</button>
                    <button type="submit" id="btnresetLingkaran" class="btn btn-danger btn-block mt-1 mb-3">Reset</button>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>


  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <script>
    $(document).ready(function(){
      $('#lebar').hide();
      $('#lingkar').hide();
      $('#rumus').on('change', function(){
        let id = $('#rumus').val();
        if(id =='')
        {
          $('#lebar').hide();
          $('#lingkar').hide();
          alert('Data Kosong')
        }
        else if(id == 1)
        {
          $('#lebar').show();
          $('#lingkar').hide();

        }
        else if(id == 2)
        {          
          $('#lebar').hide();
          $('#lingkar').show();
          
        }
      });
    // Panajang x lebar
    $('#btnjumlahlebar').on('click', function(){
      let p = $('#panjang').val();
      let l = $('#lebarr').val();
      let r = p*l;
      $('#jumlahLebar').val(r);
    })
    $('#btnresetlebar').on('click', function(){
      let p = $('#panjang').val('');
      let l = $('#lebarr').val('');
      $('#jumlahLebar').val('');
    })

  // rumus lingkar
    $('#btnjumlahLingkaran').on('click', function(){
      let n = $('#n').val();
      let l = $('#lebar_lingkaran').val();
      let r = n *l*l;
      $('#jmlhLingkaran').val(r);
    })
    $('#btnresetLingkaran').on('click', function(){
      let l = $('#lebar_lingkaran').val('');
      $('#jmlhLingkaran').val('');
    })

    });
    


  </script>
</body>
</html>