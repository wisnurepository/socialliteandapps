@extends('layouts.layout')
@section('title','Dashboard')
@section('content')
<div class="container flex flex-wrap mt-2">
  <div class="card text-center">
    <div class="card-header">
      <h5>Masukan Umur Kamu</h5>
    </div>
    <div class="card-body">
      <form action="/umur" method="post">
        @csrf
        <div class="form-group  "> 
          <input type="text" name="umur" class="form-control form-control-lg" placeholder="umur kamu">   
        </div>
        <div class="form-group  ">   
          <button type="submit" class="btn btn-success btn-block">Simpan</button>       
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
